export const toastOptions = Object.freeze({
    position:        'top-left',
    autoClose:       5000,
    hideProgressBar: false,
    closeOnClick:    true,
    pauseOnHover:    true,
    draggable:       true,
    progress:        undefined,
    theme:           'light',
});
