// core
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { toast, ToastContainer, Zoom } from 'react-toastify';

// hooks
import { useStore } from './hooks';

// Components
import { Main } from './components';

// Instruments
import { toastOptions } from './constants/toastOptions';

export const App = observer(() => {
    const { filterDaysStore } = useStore();
    const { toastErrorMessage, resetToastErrorMessage } = filterDaysStore;

    useEffect(() => {
        if (toastErrorMessage) {
            const notify = () => toast.error(toastErrorMessage, toastOptions);
            notify();
            resetToastErrorMessage();
        }
    }, [toastErrorMessage]);

    return (
        <>
            <ToastContainer newestOnTop transition = { Zoom } />
            <Main />
        </>
    );
});

