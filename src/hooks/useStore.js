// core
import { useContext } from 'react';
import { StoreContext } from '../lib/mobx/rootStoreContext';

export const useStore = () => {
    const rootStore = useContext(StoreContext);

    return rootStore;
};
