/* eslint-disable no-return-await */
/* eslint-disable no-nested-ternary */

// hooks
import { useQuery } from 'react-query';
import { useStore } from './useStore';

// api
import { api } from '../api';

// helper
import { daysFilter } from '../helpers';

export const useGetForecast = () => {
    const { filterDaysStore, filterFormStore } = useStore();
    const { filtrationProperties } = filterFormStore;
    const { setIsSpinner, setToastErrorMessage } = filterDaysStore;
    const { data, isSuccess } = useQuery('get-forecast', () => api.weather.getForeCast(), {
        onError(error) {
            setIsSpinner(true);
            setToastErrorMessage(error.message);
        },
    });

    const getData = () => {
        if (isSuccess && Array.isArray(data)) {
            if (filtrationProperties) {
                return [...daysFilter(filtrationProperties, data)].slice(0, 7);
            }

            return data.slice(0, 7);
        }

        return [];
    };

    return {
        data: getData(),
    };
};
