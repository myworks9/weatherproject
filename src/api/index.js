// core
import axios from 'axios';
import wait from 'wait';

// rootApi
const WEATHER_API = process.env.REACT_APP_WEATHER_API;

export const api = {
    weather: {
        async getForeCast() {
            const { data: response } = await axios.get(`${WEATHER_API}/forecast`);

            await wait(2000);

            return response?.data;
        },
    },
};
