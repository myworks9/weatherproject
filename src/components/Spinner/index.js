// core
import React from 'react';

export const Spinner = () => {
    return (
        <div className = 'styles_spinner__1uiLu'>
            <div aria-busy = { true }>
                <div className = 'react-spinner-loader-svg'>
                    <svg
                        id = 'triangle' viewBox = '-3 -4 39 39'
                        aria-label = 'audio-loading'>
                        <polygon
                            fill = 'transparent' stroke = '#FFF'
                            strokeWidth = '1' points = '16,0 32,32 0,32'></polygon>
                    </svg>
                </div>
            </div>
        </div>
    );
};
