// core
import cx from 'classnames';
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import { observer } from 'mobx-react-lite';
import React from 'react';

// hooks
import { useStore } from '../../../hooks';

export const Day = observer(({ day }) => {
    const { filterDaysStore } = useStore();
    const { setSelectedDay, selectedDay } = filterDaysStore;

    const activeDay = cx(`day ${day.type}`, {
        selected: selectedDay?.id === day.id,
    });

    const setSelectedDayValueHandler = () => {
        setSelectedDay(day);
    };

    return (
        <div onClick = { setSelectedDayValueHandler } className = { activeDay }>
            <p>{ day.day && format(day.day, 'eeee', {
                locale: ru,
            }) }</p>
            <span>{ day.temperature }</span>
        </div>
    );
});
