/* eslint-disable no-nested-ternary */
// core
import { observer } from 'mobx-react-lite';
import React from 'react';

// components
import { Day } from './Day';

export const Forecast = observer(({ filteredDays, isSpinner, errorMessage }) => {
    const daysJSX = filteredDays?.map((day) => <Day key = { day.id } day = { day } />);
    const getDaysJSX = () => {
        if (daysJSX?.length > 0) {
            return daysJSX;
        }
        if (errorMessage) {
            return <p className = 'message'>{ errorMessage }</p>;
        }
        if (!isSpinner) {
            return <p className = 'message'>По заданным критериям нет доступных дней!</p>;
        }

        return null;
    };

    return (
        <div className = 'forecast'>
            { getDaysJSX() }
        </div>
    );
});
