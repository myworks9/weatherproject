// core
import { format } from 'date-fns';
import { ru } from 'date-fns/locale';
import { observer } from 'mobx-react-lite';
import React from 'react';

export const Head = observer(({ day, type }) => {
    return (
        <div className = 'head'>
            <div className = { `icon ${type}` }></div>
            <div className = 'current-date'>
                <p> { day && format(day, 'eeee', {
                    locale: ru,
                }) }</p>
                <span>{ day && format(day, 'd MMMM', {
                    locale: ru,
                }) }</span>
            </div>
        </div>

    );
});

