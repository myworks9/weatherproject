// core
import _ from 'lodash';
import { observer } from 'mobx-react-lite';
import React from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useStore } from '../../hooks';

// components
import { Input } from '../elements';

export const Filter = observer(() => {
    const { filterFormStore, filterDaysStore } = useStore();
    const {
        isFilteredForm, setIsFilteredForm, resetForm, setFiltrationProperties,
    } = filterFormStore;
    const form = useForm({
        mode:          'onChange',
        defaultValues: {
            weather: null,
            maxTemp: null,
            minTemp: null,
        },
    });

    const submit = form.handleSubmit((filterData) => {
        setFiltrationProperties(filterData);
    });

    const resetFormHandler = () => {
        resetForm();
        form.reset();
    };

    const disableFormHandler = () => {
        setIsFilteredForm(true);
    };

    return (
        <form onSubmit = { submit } className = 'filter' >
            <Input
                type = 'radio'
                value = 'cloudy' label = 'Облачно'
                register = { form.register('weather') } />
            <Input
                type = 'radio'
                value = 'sunny' label = 'Солнечно'
                register = { form.register('weather') } />
            <Input
                type = 'number' label = 'Минимальная температура'
                register = { form.register('minTemp') } />
            <Input
                type = 'number' label = 'Максимальная температура'
                register = { form.register('maxTemp') } />

            { isFilteredForm ? (
                <button onClick = { resetFormHandler }>Сбросить</button>
            ) : (
                <button
                    onClick = { disableFormHandler }
                    disabled = { _.isEqual(form.formState.defaultValues, form.watch()) }>
                    Отфильтровать
                </button>
            ) }
        </form>
    );
});

