// core
import { observer } from 'mobx-react-lite';
import React from 'react';

export const CurrentWeather = observer(({ rainProbability, temperature, humidity }) => {
    return (
        <div className = 'current-weather'>
            <p className = 'temperature'>{ temperature }</p>
            <p className = 'meta'>
                <span className = 'rainy'>{ `${rainProbability}%` }</span>
                <span className = 'humidity'>{ `${humidity}%` }</span>
            </p>
        </div>
    );
});

