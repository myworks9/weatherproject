// core
import cx from 'classnames';
import { observer } from 'mobx-react-lite';

// hooks
import { useStore } from '../../../hooks';

export const Input = observer((props) => {
    const { filterFormStore } = useStore();
    const { isFilteredForm, activeCheckBox, setActiveCheckBox } = filterFormStore;
    const activeClassOfCheckBox = cx('radio', {
        selected: props.value === activeCheckBox,
    });
    const input = (
        <input
            disabled = { isFilteredForm }
            type = { props.type }
            value = { props.value }
            { ... props.register } />
    );

    return (
        props.type === 'radio' ? (
            <label
                onClick = { !isFilteredForm ? () => { setActiveCheckBox(props.value); } : null }
                className = { activeClassOfCheckBox }>
                { props.label }
                { input }
            </label>
        )
            : (
                <p className = 'custom-input' >
                    <label>
                        { props.label }
                    </label>
                    { input }
                </p>
            )
    );
});

Input.defaultProps = {
    type: 'text',
    tag:  'input',
};
