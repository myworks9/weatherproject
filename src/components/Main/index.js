// core
import _ from 'lodash';
import { runInAction } from 'mobx';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';

// hooks
import { useGetForecast, useStore } from '../../hooks';

// components
import {
    CurrentWeather, Filter, Forecast, Head, Spinner,
} from '..';

export const Main = observer(() => {
    const { data: days } = useGetForecast();
    const { filterDaysStore } = useStore();
    const {
        filteredDays,
        setFilteredDaysSuccess,
        selectedDay,
        resetFilteredDays,
        isSpinner,
        errorMessage,
    } = filterDaysStore;

    useEffect(() => {
        if (days) {
            runInAction(() => {
                if (days.length > 0 && !_.isEqual(filteredDays, days)) {
                    setFilteredDaysSuccess(days);
                }
            });
            if (days.length === 0) {
                resetFilteredDays();
            }
        }
    }, [days]);

    return (
        <main>
            { !isSpinner && !errorMessage && <Filter /> }

            { filteredDays?.length > 0 ? (
                <>
                    <Head day = { selectedDay?.day } type = { selectedDay?.type } />
                    <CurrentWeather
                        temperature = { selectedDay?.temperature }
                        humidity = { selectedDay?.humidity }
                        rainProbability = { selectedDay?.rain_probability } />
                </>
            ) : null }

            <Forecast
                errorMessage = { errorMessage }
                isSpinner = { isSpinner }
                filteredDays = { filteredDays } />
            { isSpinner && <Spinner /> }
        </main>
    );
});
