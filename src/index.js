// Core
import { configure } from 'mobx';
import { render } from 'react-dom';
import { QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

// Component
import { App } from './app';

// client
import { queryClient } from './lib';

// context
import { RootStoreContext } from './lib/mobx/rootStoreContext';

// Instruments
import 'react-toastify/dist/ReactToastify.css';
import './theme/index.scss';

configure({
    enforceActions:             'always',
    computedRequiresReaction:   true,
    observableRequiresReaction: true,
});

render(
    <RootStoreContext>
        <QueryClientProvider client = { queryClient }>
            <App />
            <ReactQueryDevtools initialIsOpen = { false } />
        </QueryClientProvider>
    </RootStoreContext>,
    document.getElementById('root'),
);
