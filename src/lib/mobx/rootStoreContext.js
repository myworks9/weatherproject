// core
import { createContext } from 'react';
import { RootStore } from '.';

// rootStore
const rootStore = new RootStore();

export const StoreContext = createContext(rootStore);

export const RootStoreContext = ({ children }) => {
    return (
        <StoreContext.Provider value = { rootStore } >
            { children }
        </StoreContext.Provider>
    );
};
