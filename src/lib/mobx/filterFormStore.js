/* eslint-disable no-tabs */
/* eslint-disable no-mixed-spaces-and-tabs */

// core
import { makeAutoObservable } from 'mobx';

class FilterFormStore {
	_activeCheckBox = '';
	_isFilteredForm = false;
	_filtrationProperties = null;

	constructor(rootStore) {
	    makeAutoObservable(this, { rootStore: false }, {
	        autoBind: true,
	    });
	    this.rootStore = rootStore;
	}

	get isFilteredForm() {
	    return this._isFilteredForm;
	}

	setIsFilteredForm(isFiltered) {
	    this._isFilteredForm = isFiltered;
	}

	get activeCheckBox() {
	    return this._activeCheckBox;
	}

	setActiveCheckBox(newCheckBox) {
	    this._activeCheckBox = newCheckBox;
	}

	resetForm() {
	    this._activeCheckBox = '';
	    this._isFilteredForm = false;
	    this._filtrationProperties = null;
	}

	get filtrationProperties() {
	    return this._filtrationProperties;
	}

	setFiltrationProperties(property) {
	    this._filtrationProperties = property;
	}
}

export { FilterFormStore };

