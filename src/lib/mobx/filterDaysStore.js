/* eslint-disable no-tabs */
/* eslint-disable no-mixed-spaces-and-tabs */

// core
import { makeAutoObservable } from 'mobx';

class FilterDaysStore {
	_filteredDays = null;
	_selectedDay = null;
	_isSpinner = true;
	_errorMessage = null;
	_toastErrorMessage = null;

	constructor(rootStore) {
	    makeAutoObservable(this, { rootStore: false }, {
	        autoBind: true,
	    });
	    this.rootStore = rootStore;
	}

	get errorMessage() {
	    return this._errorMessage;
	}

	get toastErrorMessage() {
	    return this._toastErrorMessage;
	}

	setToastErrorMessage(newError) {
	    if (newError) {
	        this._toastErrorMessage = newError;
	        this._errorMessage = newError;
	        this._isSpinner = false;
	    }
	}

	resetToastErrorMessage() {
	    this._toastErrorMessage = null;
	}

	get isSpinner() {
	    return this._isSpinner;
	}

	setIsSpinner(decision) {
	    this._isSpinner = decision;
	}

	get selectedDay() {
	    return this._selectedDay;
	}

	setSelectedDay(newDay) {
	    this._selectedDay = newDay;
	}

	get filteredDays() {
	    return this._filteredDays;
	}

	setFilteredDaysSuccess(newDays) {
	    if (newDays.length > 0) {
	        this._errorMessage = null;
	        this._isSpinner = false;
	        this._filteredDays = [...newDays];
	        this.setSelectedDay(this.filteredDays[ 0 ]);
	    }
	}

	resetFilteredDays() {
	    this._filteredDays = null;
	}
}

export { FilterDaysStore };

