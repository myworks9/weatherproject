/* eslint-disable no-tabs */
/* eslint-disable no-mixed-spaces-and-tabs */

// core
import { makeAutoObservable } from 'mobx';
import { FilterDaysStore } from './filterDaysStore';

// stores
import { FilterFormStore } from './filterFormStore';

class RootStore {
	filterFormStore = null;
	filterDaysStore = null;

	constructor() {
	    makeAutoObservable(this, {}, {
	        autoBind: true,
	    });
	    this.filterDaysStore = new FilterDaysStore(this);
	    this.filterFormStore = new FilterFormStore(this);
	}
}

export { RootStore };

