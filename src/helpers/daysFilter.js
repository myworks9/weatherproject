/* eslint-disable array-bracket-newline */

export const daysFilter = (filtrationProp, arr) => {
    let filteredDays = [...arr];
    if (filtrationProp?.weather) {
        filteredDays = [
            ...filteredDays.filter((day) => day.type === filtrationProp.weather),
        ];
    }
    if (filtrationProp?.minTemp) {
        filteredDays = [
            ...filteredDays.filter((day) => day.temperature >= filtrationProp.minTemp),
        ];
    }
    if (filtrationProp?.maxTemp) {
        filteredDays = [
            ...filteredDays.filter((day) => day.temperature <= filtrationProp.maxTemp),
        ];
    }

    return filteredDays;
};
